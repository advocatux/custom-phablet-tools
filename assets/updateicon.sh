#!/bin/bash

name_folder_icons=$1

sudo mount -o rw,remount /

list_icons=` 
cd /usr/share/applications/
grep -P "^Icon=.+(png|svg)$" *.desktop | sed 's/.desktop:Icon=/":"/;s/^/"/;s/$/",/'

cd ~/.local/share/applications
grep -P "^Icon=.+(png|svg)$" *.desktop | sed 's/.desktop:Icon=/":"/;s/^/"/;s/$/",/'
`

list_icons_name=(`grep -Po '(?<=")[^"]+(?=":)' <<<$list_icons`)
list_icons_path=(`grep -Po '(?<=:")[^"]+(?=")' <<<$list_icons`)

cd assets

#Création du dossier de sauvegarde des icones d'origine
if [ ! -d "backup" ]; then
    mkdir backup
fi

#Création d'un dossier temporaire pour le pack d'icone souhaité
if [ ! -d "$name_folder_icons" ]; then
    mkdir $name_folder_icons
fi


#Boucle pour chaque app installé
for index in ${!list_icons_name[*]}; do 

    list_icons_ext=${list_icons_path[$index]##*.}
    list_icons_rename=${list_icons_name[$index]%%_*}
    
    #Copie des fichiers d'origine dans dossier backup si elles n'existe pas
    if [ ! -f "backup/"$list_icons_rename.$list_icons_ext ]; then
        sudo cp ${list_icons_path[$index]} backup/$list_icons_rename.$list_icons_ext
    fi
    
    # DL DES FICHIERS DISTANT
    cd $name_folder_icons
    wget https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/$name_folder_icons/$list_icons_rename.$list_icons_ext
    
    #Copie des icone si existe
    if [ -f "$list_icons_rename.$list_icons_ext" ]; then
            sudo cp $list_icons_rename.$list_icons_ext ${list_icons_path[$index]}
    fi  
    cd ..
    
done

#Suppression du dossier du pack d'icone téléchargé
if [ -d "$name_folder_icons" ]; then
    rm -rf $name_folder_icons
fi

#Suppression du backup si application d'un backup online
if [ $name_folder_icons == "default" ]; then
    rm -rf backup
fi

 sudo mount -o ro,remount /