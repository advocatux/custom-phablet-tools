#!/bin/bash

sudo mount -o rw,remount /

list_icons=` 
cd /usr/share/applications/
grep -P "^Icon=.+(png|svg)$" *.desktop | sed 's/.desktop:Icon=/":"/;s/^/"/;s/$/",/'

cd ~/.local/share/applications
grep -P "^Icon=.+(png|svg)$" *.desktop | sed 's/.desktop:Icon=/":"/;s/^/"/;s/$/",/'
`

list_icons_name=(`grep -Po '(?<=")[^"]+(?=":)' <<<$list_icons`)
list_icons_path=(`grep -Po '(?<=:")[^"]+(?=")' <<<$list_icons`)

cd assets/backup


#Boucle pour chaque app installé
for index in ${!list_icons_name[*]}; do 

    list_icons_ext=${list_icons_path[$index]##*.}
    list_icons_rename=${list_icons_name[$index]%%_*}
    
    #Copie des icone si existe
    if [ -f "$list_icons_rename.$list_icons_ext" ]; then
            sudo cp $list_icons_rename.$list_icons_ext ${list_icons_path[$index]}
    fi  
  
done

 sudo mount -o ro,remount /