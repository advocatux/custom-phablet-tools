msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: POEditor.com\n"
"Project-Id-Version: Custom Phablet Tools\n"
"Language: fr\n"

#: ../qml/Main.qml:55 ../qml/Wallpaper.qml:17 ../qml/Icons.qml:19
#: ../qml/IconsView.qml:17 ../qml/WallpaperView.qml:18
#: custom-phablet-tools.desktop.in.h:1
#, fuzzy
msgid "Custom phablet tools"
msgstr "Outils de personnalisation"

#: ../qml/Main.qml:108 ../qml/Wallpaper.qml:63 ../qml/Icons.qml:65
#, fuzzy
msgid "Search"
msgstr "Chercher"

#: ../qml/Main.qml:113 ../qml/Wallpaper.qml:68 ../qml/Icons.qml:70
msgid "Close"
msgstr "Fermer"

#: ../qml/Main.qml:60 ../qml/Wallpaper.qml:29 ../qml/Icons.qml:31
msgid "Wallpapers"
msgstr "Fonds d'écran"

#. Patientez s'il vous plaît
#: ../qml/IconsView.qml:113 ../qml/WallpaperView.qml:114
msgid "Wait please"
msgstr "Merci de patienter"

#: ../qml/IconsView.qml:127
msgid "Apply Icons"
msgstr "Appliquer"

#: ../qml/IconsView.qml:128
msgid "Do you want to apply the icons now?"
msgstr "Voulez-vous appliquer les nouvelles icônes maintenant ?"

#: ../qml/IconsView.qml:129 ../qml/IconsView.qml:209
msgid "All currently open apps will be closed."
msgstr "Toutes les applications actuellement ouvertes seront fermées."

#: ../qml/IconsView.qml:130 ../qml/IconsView.qml:210
msgid "Save all your work before continuing!"
msgstr "Sauvegardez votre travail avant de continuer !"

#: ../qml/IconsView.qml:133 ../qml/IconsView.qml:213
#: ../qml/WallpaperView.qml:132
msgid "Cancel"
msgstr "Annuler"

#: ../qml/IconsView.qml:138
msgid "Apply icons"
msgstr "Appliquer"

#: ../qml/IconsView.qml:168 ../qml/WallpaperView.qml:168
msgid "Install"
msgstr "Installer"

#: ../qml/IconsView.qml:207
msgid "Restore Icons"
msgstr "Restaurer les icônes"

#: ../qml/IconsView.qml:208
msgid "Do you want to restore the icons now?"
msgstr "Voulez-vous restaurer les icônes maintenant ?"

#: ../qml/IconsView.qml:218
msgid "Restore icons"
msgstr "Restaurer les icônes"

#: ../qml/IconsView.qml:250
msgid "Restore"
msgstr "Restaurer"

#: ../qml/IconsView.qml:272 ../qml/WallpaperView.qml:188
msgid "by"
msgstr "par"

#: ../qml/IconsView.qml:278 ../qml/WallpaperView.qml:193
msgid "Item details"
msgstr "Détails de l'objet"

#: ../qml/IconsView.qml:282 ../qml/WallpaperView.qml:197
msgid "Last update"
msgstr "Dernière mise à jour"

#: ../qml/IconsView.qml:283 ../qml/WallpaperView.qml:198
msgid "categories"
msgstr "catégories"

#: ../qml/WallpaperView.qml:128 ../qml/WallpaperView.qml:137
msgid "Apply Wallpaper"
msgstr "Appliquer le fond d’écran"

#: ../qml/WallpaperView.qml:129
msgid "Do you want to apply the wallpaper now?"
msgstr "Souhaitez-vous changer le fond d’écran maintenant ?"

#: ../qml/Main.qml:67 ../qml/Main.qml:290 ../qml/Wallpaper.qml:36
#: ../qml/Icons.qml:38
msgid "Icons"
msgstr "Icônes"

#: ../qml/Main.qml:266
msgid "New pack of icons"
msgstr "Nouveau groupe d'icônes"

#: ../qml/Main.qml:319
msgid "New wallpapers"
msgstr "Nouveaux fonds d'écran"

#: ../qml/Main.qml:344
msgid "Wallpaper"
msgstr "Fond d'écran"

#: ../qml/Wallpaper.qml:22 ../qml/Icons.qml:24
msgid "Home"
msgstr "Accueil"

#: ../qml/Icons.qml:48
msgid "search"
msgstr "chercher"

#: ../qml/About.qml:10
msgid "About"
msgstr "À propos"

