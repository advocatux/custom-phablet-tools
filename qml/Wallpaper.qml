import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Terminalaccess 1.0
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 0.1
import "components"

Page {
    id: wallpapersPage
        
        header: state == "default" ? defaultHeader : searchPageHeader
        state: "default"
            
    PageHeader {
        id: defaultHeader
        title: i18n.tr('Custom phablet tools')

        leadingActionBar.actions: [
                Action {
                    iconName: "home"
                    text: i18n.tr("Home")
                    onTriggered: {
                        pageStack.pop();
                    }
                }, 
                Action {
                    iconName: "import-image"
                    text: i18n.tr("Wallpapers")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Wallpaper.qml"));
                    }
                },
                Action {
                    iconName: "keypad"
                    text: i18n.tr("Icons")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Icons.qml"));
                    }
                }
        ]
        trailingActionBar {
            actions: [
                Action {
                    iconName: "search"
                    text: "search"
                    onTriggered: {
                        wallpapersPage.state = "search"
                    }
                }/*,
                Action {
                    iconName: "filter"
                    text: "filter"
                }*/
           ]
           numberOfSlots: 2
        }
    }
    
    PageHeader {
        id: searchPageHeader
        visible: wallpapersPage.state == "search"
        title: i18n.tr("Search")
        leadingActionBar {
            actions: [
                Action {
                    id: closePageAction
                    text: i18n.tr("Close")
                    iconName: "back"
                    onTriggered: {
                        wallpapersPage.state = "default"
                        jsonwallpaper.query = "$[*]"

                    }
                }

            ]
        }/*
            trailingActionBar {
                actions: [
                    Action {
                        iconName: "filter"
                        text: "filter"
                    }
               ]
               numberOfSlots: 1
            }
        */
        contents: Rectangle {
            color: "#fff"
            anchors {
                left: parent.left
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            TextField {
                id: searchField
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
                primaryItem: Icon {
                    anchors.leftMargin: units.gu(0.2)
                    height: parent.height*0.5
                    width: height
                    name: "find"
                }
                hasClearButton: true
                inputMethodHints: Qt.ImhNoPredictiveText
                onVisibleChanged: {
                    if (visible) {
                        forceActiveFocus()
                    }
                }
                onTextChanged: {
                    if (searchField.text == ""){
                        jsonwallpaper.query = "$[*]"
                    }else{
                        jsonwallpaper.query = "$[?(/"+searchField.text+"/i.test(@.name))]"
                    }
                }
            }
        }
    }    
    
    Rectangle {
        id:main
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: wallpapersPage.header.bottom
        }
        color: "#111111"

            
    ActivityIndicator {
        id: activity
        running: true
        anchors.centerIn: parent
    }
        
    GridView {
        id: gview
        anchors.fill: parent
        anchors {
            rightMargin: units.gu(2)
            leftMargin: units.gu(2)
            topMargin: units.gu(2)
        }         
        cellHeight: iconbasesize+units.gu(10)
        property real iconbasesize: units.gu(12)
        cellWidth: Math.floor(width/Math.floor(width/iconbasesize))
            
        focus: true
        model: jsonwallpaper.model

            
            JSONListModel {
                id: jsonwallpaper
                source: "https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/wallpapers/wallpaper.json"
                query: "$[*]"
            }          
         
        delegate: Rectangle {
                    width: gview.cellWidth 
                    height: gview.iconbasesize 
                    color: "transparent"
                
                    Item {
                        width: units.gu(12)
                        height: units.gu(20)
                            anchors.horizontalCenter: parent.horizontalCenter

                        Image {
                            id: imgIcons
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            height: parent.height
                            source: Qt.resolvedUrl("https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/wallpapers/"+model.type+"/"+model.realname)
                            visible: false
                        }
                        
                        UbuntuShape {
                            source: imgIcons
                            width: parent.width
                            height: parent.height
                            radius : "medium"
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                pageStack.push(Qt.resolvedUrl("WallpaperView.qml"), {"wallpaperName": model.name, "wallpaperType": model.type, "wallpaperRealname": model.realname, "wallpaperAuthors": model.author, "wallpaperUpdate": model.update, "wallpaperCategories": ""});
                            }
                        }
                        Component.onCompleted: {
                            activity.running = false
                        }
                    } // Item
            }// delegate Rectangle
        
        }                 
                
                
                
                

    } //rectangle
} //page
